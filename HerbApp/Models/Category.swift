//
//  Category.swift
//  HerbApp
//
//  Created by Shoaib Ismail on 19/07/2018.
//  Copyright © 2018 Emblem Technologies. All rights reserved.
//

import Foundation

// MARK: - Category Modal

class Category {
    var description: String = DefaultValue.string
    var id: Int = DefaultValue.number
    var image: String =  DefaultValue.string
    var name: String = DefaultValue.string
    
    var stemsList = Array<Stem>()
    
    init(_ productJSonObject: Dictionary<String, Any>?) {
        if(productJSonObject != nil){
            parseJsonData(productJSonObject: productJSonObject!)
        }
    }
    
    func parseJsonData(productJSonObject: Dictionary<String, Any>) {
        description = productJSonObject[APIKey.description] as? String ?? DefaultValue.string
        id = productJSonObject[APIKey.id] as? Int ?? DefaultValue.number
        image = productJSonObject[APIKey.image] as? String ?? DefaultValue.string
        name = productJSonObject[APIKey.name] as? String ?? DefaultValue.string
        
    }
}
