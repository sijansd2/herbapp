//
//  Product.swift
//  HerbApp
//
//  Created by Shoaib Ismail on 19/07/2018.
//  Copyright © 2018 Emblem Technologies. All rights reserved.
//

import Foundation

// MARK: - Product Model

class Stem {
    var description: String = DefaultValue.string
    var id: Int = DefaultValue.number
    var imagesList = Array<Image>()
    var name: String = DefaultValue.string
    var thc: String = DefaultValue.string
    
    var categoryId: Int = DefaultValue.number
    var categoryName: String = DefaultValue.string
    
    init(_ productJSonObject: Dictionary<String, Any>?) {
        if(productJSonObject != nil){
            parseJsonData(productJSonObject: productJSonObject!)
        }
    }
    
    func parseJsonData(productJSonObject: Dictionary<String, Any>) {
        description = productJSonObject[APIKey.description] as? String ?? DefaultValue.string
        id = productJSonObject[APIKey.id] as? Int ?? DefaultValue.number
        if let imageList: Array<Dictionary<String,Any>> = productJSonObject[APIKey.image] as? Array<Dictionary<String, Any>>{
            
            self.imagesList.removeAll()
            imageList.forEach { (imageDic) in
                self.imagesList.append(Image(imageDic))
            }
        }
        name = productJSonObject[APIKey.name] as? String ?? DefaultValue.string
        thc = productJSonObject[APIKey.thc] as? String ?? DefaultValue.string
        categoryId = productJSonObject[APIKey.categoryId] as? Int ?? DefaultValue.number
        categoryName = productJSonObject[APIKey.categoryName] as? String ?? DefaultValue.string
    }
    
}

class Image{
    var id: Int = DefaultValue.number
    var imageURL: String = DefaultValue.string
    
    init(_ productJSonObject: Dictionary<String, Any>?) {
        if(productJSonObject != nil){
            parseJsonData(productJSonObject: productJSonObject!)
        }
    }
    
    func parseJsonData(productJSonObject: Dictionary<String, Any>) {
        id = productJSonObject[APIKey.id] as? Int ?? DefaultValue.number
        imageURL = productJSonObject[APIKey.image] as? String ?? DefaultValue.string
    }
}
