//
//  APIHandler.swift
//  MJDelivery
//
//  Created by Ramzan on 22/01/2018.
//  Copyright © 2018 Ramzan. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration


protocol ImageUploadProtocol {
    func uploadProgress(progress: Float)
}

class APIHandler: NSObject, URLSessionDelegate, URLSessionTaskDelegate, URLSessionDataDelegate{
    
    //MARK: -Properties
    static let shared = APIHandler()
    var delegate: ImageUploadProtocol!
    let printDebugResponses = true
    
    //MARK: -Setup
    override init(){
        // Do any additional setup before init
        super.init()

    }
    
    private func resumeDataTaskWith(request: NSMutableURLRequest,onSuccess: @escaping (_ response: Any) -> Swift.Void, onError: @escaping (_ message: String,_ code: NSInteger, _ response: Any?) -> Swift.Void){
        
        let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
            
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                if error != nil{
                    onError((error?.localizedDescription)!, (error! as NSError).code, nil)
                    self.printFailerAPILogs(requestURL: (request.url?.absoluteString)!, requestParams: request.httpBody, requestResponse: error as Any)
                    
                    return
                }
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                    
                    if let jsonDict = json as? Dictionary<String, Any>{
                        self.printFailerAPILogs(requestURL: (request.url?.absoluteString)!, requestParams: request.httpBody, requestResponse: jsonDict as Any)
                        
                        if let error = jsonDict[APIKey.REST_ERROR_CODE] as? NSNumber{
                            
                            onError("Something Went Wrong",error.intValue,json)
                            
                            return
                        }else if let error = jsonDict[APIKey.REST_ERROR_CODE] as? String{
                            
                            onError(error,0,json)
                            return
                        }
                    }
                    
                    onSuccess(json)
                    if(self.printDebugResponses && request.httpBody != nil){
                        self.printFailerAPILogs(requestURL: (request.url?.absoluteString)!, requestParams: request.httpBody!, requestResponse: json as Any)
                    }
                    
                } catch let error as NSError {
                    onError(error.localizedDescription, error.code, nil)
                    self.printFailerAPILogs(requestURL: (request.url?.absoluteString)!, requestParams: request.httpBody!, requestResponse: error as Any)
                }
            }
            
        }
        
        task.resume()
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
    }
    
    private func resumeDataTaskWith(request: NSMutableURLRequest,onSuccess: @escaping (_ response: Any) -> Swift.Void, onError: @escaping (_ response: Any,_ message: String,_ code: NSInteger, _ response: Any?) -> Swift.Void){
        
        let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
            
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                if error != nil{
                    onError((Any).self,(error?.localizedDescription)!, (error! as NSError).code, nil)
                    self.printFailerAPILogs(requestURL: (request.url?.absoluteString)!, requestParams: request.httpBody!, requestResponse: error as Any)
                    
                    return
                }
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                    
                    if let jsonDict = json as? NSDictionary{
                        if let error = jsonDict.object(forKey: APIKey.REST_ERROR_CODE) as? NSNumber{
                            
                            onError(json,"Something Went Wrong",error.intValue, json)
                            self.printFailerAPILogs(requestURL: (request.url?.absoluteString)!, requestParams: request.httpBody!, requestResponse: jsonDict as Any)
                            return
                        }else if let error = jsonDict.object(forKey: APIKey.REST_ERROR_CODE) as? String{
                            self.printFailerAPILogs(requestURL: (request.url?.absoluteString)!, requestParams: request.httpBody!, requestResponse: jsonDict as Any)
                            onError(json,error,0, json)
                            return
                        }
                    }
                    
                    onSuccess(json)
                    if(self.printDebugResponses && request.httpBody != nil){
                        self.printFailerAPILogs(requestURL: (request.url?.absoluteString)!, requestParams: request.httpBody!, requestResponse: json as Any)
                    }
                    
                } catch let error as NSError {
                    onError((Any).self,error.localizedDescription, error.code, nil)
                    self.printFailerAPILogs(requestURL: (request.url?.absoluteString)!, requestParams: request.httpBody!, requestResponse: error as Any)
                }
            }
            
        }
        
        task.resume()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    private func printFailerAPILogs(requestURL: String, requestParams: Data?, requestResponse: Any){
        
        print("requestURL: \(requestURL)")
        do{
            if(requestParams != nil){
                let params = try JSONSerialization.jsonObject(with: requestParams!, options: [])
                print("requestParams: \(params)")
            }else{
                print("requestParams: nil")
            }
        } catch let error as NSError{
            print("requestParams: \(error)")
            
        }
        print("requestResponse: \(requestResponse)")
        
        
    }
    
    private func getRequest(urlString: String,onSuccess: @escaping (_ response: Any) -> Swift.Void, onError: @escaping (_ message: String,_ code: NSInteger, _ response: Any?) -> Swift.Void){
        
        let url = NSURL(string: APIUrl.BASE_URL_STRING+urlString)!
        let request = NSMutableURLRequest(url: url as URL)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "GET"
        
        self.resumeDataTaskWith(request: request, onSuccess: onSuccess, onError: onError)
    }
    
    private func postRequest(urlString: String, params: Dictionary<String, Any>, onSuccess: @escaping (_ response: Any) -> Swift.Void, onError: @escaping (_ message: String,_ code: NSInteger, _ response: Any) -> Swift.Void){
        
        var params = params
        params[APIKey.versionNumber] = Constants.API_VERSION
        if let jsonData = try? JSONSerialization.data(withJSONObject: params, options: []) {
            
            let url = NSURL(string: APIUrl.BASE_URL_STRING+urlString)!
            let request = NSMutableURLRequest(url: url as URL)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            request.httpBody = jsonData
            
            self.resumeDataTaskWith(request: request, onSuccess: onSuccess, onError: onError)
            
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        let uploadProgress:Float = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
        if(delegate != nil){
            delegate.uploadProgress(progress: uploadProgress)
        }
    }
    
    //
    //
    //    func URLSession(session: NSURLSession, task: NSURLSessionTask,bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64)
    //    {
    //        println("didSendBodyData")
    //        var uploadProgress:Float = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
    //        imageUploadProgressView.progress = uploadProgress
    //        let progressPercent = Int(uploadProgress*100)
    //        progressLabel.text = "\(progressPercent)%"
    //        println(uploadProgress)
    //    }
    //
    
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func appendString(_ string: String)->Data {
        return string.data(using: String.Encoding.utf8, allowLossyConversion: true)!
    }
    
    //MARK: -API Methods
    
    func getCategories(onSuccess: @escaping (_ response: Any) -> Swift.Void, onError: @escaping (_ message: String,_ code: NSInteger, _ response: Any) -> Swift.Void){
        
        let params = Dictionary<String, Any>()
        
        postRequest(urlString: APIUrl.getCategories, params: params, onSuccess: onSuccess, onError: onError)
    }
    
    func getCategoryProducts(categoryId: Int, onSuccess: @escaping (_ response: Any) -> Swift.Void, onError: @escaping (_ message: String,_ code: NSInteger, _ response: Any) -> Swift.Void){
        
        var params = Dictionary<String, Any>()
        params[APIKey.categoryId] = categoryId
        
        postRequest(urlString: APIUrl.getCategoryProducts, params: params, onSuccess: onSuccess, onError: onError)
    }
    
    func getStems(searchString: String, onSuccess: @escaping (_ response: Any) -> Swift.Void, onError: @escaping (_ message: String,_ code: NSInteger, _ response: Any) -> Swift.Void){
        
        var params = Dictionary<String, Any>()
        params[APIKey.searchString] = searchString
        
        postRequest(urlString: APIUrl.getStems, params: params, onSuccess: onSuccess, onError: onError)
    }

    
}
