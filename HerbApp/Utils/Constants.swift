//
//  Constants.swift
//  MJDriver
//
//  Created by Shahbaz Saleem on 1/25/18.
//  Copyright © 2018 YamYamMobile. All rights reserved.
//

import UIKit

class Constants: NSObject {
    static let API_VERSION = Float(1.0)
}

enum Storyboard{
    static let main = "Main"
}

enum UserDefaultsKey{
    static let user = "user"
    static let fcmToken = "fcm_token"
    static let errorPlaceHolder = "errorPlaceHolder"
}

enum APIKey{
    static let REST_ERROR_CODE = "rest_error_code"
    static let versionNumber = "version_number"
    static let success = "success"
    static let description = "description"
    static let id = "id"
    static let image = "image"
    static let name = "name"
    static let thc = "thc"
    static let items = "items"
    static let categoryId = "category_id"
    static let categoryName = "category_name"
    static let searchString = "search_string"


}

enum APIUrl{
    static var BASE_URL_STRING = PROD_URL_STRING
    static let PROD_URL_STRING = "http://35.172.179.73:8080/"
    
    static let getCategories = "client/get_categories"
    static let getStems = "client/get_stems"
    static let getCategoryProducts = "client/get_category_stem"
    
    
}

enum DefaultValue{
    static let string = ""
    static let number = 0
    static let amount = "$0"
    static let dateFormat = "MMM dd, yyyy"
    static let dateFormatForAPI = "yyyy-M-dd"
    static let dateWithoutYearFormat = "MMM dd"
}

enum ErrorCode: Int{
    case undefinedError = 1, deauthenticated, duplicate, doesNotExist
    case internetError = -1009
}

