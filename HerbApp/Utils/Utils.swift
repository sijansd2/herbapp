//
//  Utils.swift
//  HerbApp
//
//  Created by Shoaib Ismail on 19/07/2018.
//  Copyright © 2018 Emblem Technologies. All rights reserved.
//

import UIKit
import SDWebImage

class Utils {
    static let shared = Utils()
    
    var isiPhone: Bool{
        return (UIDevice.current.userInterfaceIdiom == .phone)
    }
    
    func loadImage(imageView: UIImageView, urlString: String, placeHolderImageString: String?)->Swift.Void{
        let placeHolder = UIImage(named: placeHolderImageString ?? "")
        imageView.image = placeHolder
        
        if(urlString != "" && !urlString.isEmpty && urlString != "Not Available"){
            
            let encodedString:String = urlString.replacingOccurrences(of: " ", with: "%20")
            
            imageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: placeHolderImageString ?? ""))
            
        }
        
        
    }
    
    func getCategoryByCategoryId(categoryId: Int) -> Category?{
        for case let category in MainViewController.categoriesList{
            if(category.id == categoryId){
                return category
            }
        }
        return nil
    }
}

extension UILabel {
    func textHeight(withWidth width: CGFloat) -> CGFloat {
        guard let text = text else {
            return 0
        }
        return text.height(withWidth: width, font: font)
    }
    
    func attributedTextHeight(withWidth width: CGFloat) -> CGFloat {
        guard let attributedText = attributedText else {
            return 0
        }
        return attributedText.height(withWidth: width)
    }
}

extension String {
    func height(withWidth width: CGFloat, font: UIFont) -> CGFloat {
        let maxSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let actualSize = self.boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], attributes: [.font : font], context: nil)
        return actualSize.height
    }
}

extension NSAttributedString {
    func height(withWidth width: CGFloat) -> CGFloat {
        let maxSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let actualSize = boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], context: nil)
        return actualSize.height
    }
}


extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}


