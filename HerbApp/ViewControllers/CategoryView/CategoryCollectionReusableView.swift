//
//  CategoryCollectionReusableView.swift
//  HerbApp
//
//  Created by Shoaib Ismail on 18/07/2018.
//  Copyright © 2018 Emblem Technologies. All rights reserved.
//

import UIKit
protocol CategoryCollectionReusableViewProtocol {
    func updateHeaderHeight(height: CGFloat)
}
class CategoryCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var ivCategory: UIImageView!
    @IBOutlet weak var labelCategoryName: UILabel!
    @IBOutlet weak var labelCategoryDiscription: UILabel!
    @IBOutlet weak var lblStrainsHeading: UILabel!
    
    var delegate: CategoryCollectionReusableViewProtocol?
    
    func populateData(category: Category, stemsList: Array<Stem>) {
        Utils.shared.loadImage(imageView: ivCategory, urlString: category.image, placeHolderImageString: "PlaceHolderBanner")
        labelCategoryName.text = category.name
        labelCategoryDiscription.text = category.description
        
        if(stemsList.count == 1){
            lblStrainsHeading.text = "1 Strain"
        }else if(stemsList.count > 0){
            lblStrainsHeading.text = "\(stemsList.count) Strains"
        }
        
        delegate?.updateHeaderHeight(height: labelCategoryDiscription.sizeThatFits(labelCategoryDiscription.frame.size).height)
    }
    
    
}
