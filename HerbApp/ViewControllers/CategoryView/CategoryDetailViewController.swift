//
//  CategoryDetailViewController.swift
//  HerbApp
//
//  Created by Shoaib Ismail on 18/07/2018.
//  Copyright © 2018 Emblem Technologies. All rights reserved.
//

import UIKit

class CategoryDetailViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet var stemsCollectionView: UICollectionView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchContainer: UIView!
    @IBOutlet weak var searchCancelButton: UIButton!
    @IBOutlet weak var bannerBlackBG: UIView!
    
    @IBOutlet weak var searchFieldXConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchFieldYConstraint: NSLayoutConstraint!
    
    var searchViewController: SearchViewController?
    
    var collectionViewOffset = CGFloat(0)
    
    var category: Category = Category(nil)
    var stemsList = Array<Stem>()
    
    var stemToBeSent: Stem?
    var heightOfDescription: CGFloat = 0
    
    // MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ProductViewController{
            vc.delegate = self
            if(stemToBeSent != nil){
                vc.stem = stemToBeSent!
            }
        }else if let vc = segue.destination as? SearchViewController{
            searchViewController = vc
            vc.delegate = self
        }
    }
    
    // MARK: - Setup
    private func setup(){
        searchField.setLeftPaddingPoints(23)
        setCollectionViewOpacity()
        setupNavigation()
        getCategoryProductsFromRemote()
    }
    
    private func setCollectionViewOpacity(){
        stemsCollectionView.alpha = 0
    }
    
    private func setupNavigation(){
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    // MARK: - UIActions
    
    @IBAction func didSelectBackButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @IBAction func searchTextFieldEditingChanged(_ sender: UITextField) {
        
        searchViewController?.didSearchUpdated(searchString: sender.text ?? DefaultValue.string)
        
    }
    
    @IBAction func didSelectSearchCancel(_ sender: UIButton) {
        searchField.text = nil
        searchField.resignFirstResponder()
        searchViewController?.didSearchUpdated(searchString: searchField.text ?? DefaultValue.string)
        UIView.animate(withDuration: 0.3) {
            self.searchCancelButton.alpha = 0
            self.searchFieldXConstraint.constant = 0
            self.searchFieldYConstraint.constant = 20
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - APIs
    func getCategoryProductsFromRemote(){
        
        APIHandler.shared.getCategoryProducts(categoryId: category.id, onSuccess: { (response) in
            if let responseDic = response as? Dictionary<String,Any>{
                if let items = responseDic[APIKey.items] as? Array<Dictionary<String,Any>>{
                    self.stemsList.removeAll()
                    items.forEach({ (stemDic) in
                        self.stemsList.append(Stem(stemDic))
                    })
                    self.stemsCollectionView.reloadData()
                    self.perform(#selector(self.reloadMainCollection), with: nil, afterDelay: 0.3)
                    UIView.animate(withDuration: 0.4, animations: {
                        self.stemsCollectionView.alpha = 1
                    })
                }
            }
        }) { (message, errorCode, response) in
            
        }
    }
    
    @objc func reloadMainCollection(){
        self.stemsCollectionView.reloadData()
    }
    
    
}

// MARK: - ProducgtsCollectionView DataSource & Delegate

extension CategoryDetailViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate{
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: String(describing: CategoryCollectionReusableView.self), for: indexPath) as? CategoryCollectionReusableView{
            
            headerView.delegate = self
            headerView.populateData(category: category, stemsList: stemsList)
            return headerView
            
        }else{
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return CGSize.init(width: self.view.frame.width, height: 85+heightOfDescription+self.view.frame.width*(9/16))
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.stemsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ( self.view.frame.width - 40 )
        return CGSize(width: width, height: 53)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ProductCollectionViewCell.self), for: indexPath) as! ProductCollectionViewCell
        
        cell.populateCell(stem: stemsList[indexPath.item])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        stemToBeSent = stemsList[indexPath.item]
        performSegue(withIdentifier: String(describing: ProductViewController.self), sender: self)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView.contentOffset.y > self.collectionViewOffset){
            //upward
            let alpha = (1 - scrollView.contentOffset.y/100) < 0 ? 0 : (1 - scrollView.contentOffset.y/100)
            self.backButton.alpha = alpha
            self.bannerBlackBG.isHidden = true
        }else{
            //downward
            let alpha = (1 - scrollView.contentOffset.y/100) > 1 ? 1 : (1 - scrollView.contentOffset.y/100)
            self.backButton.alpha = alpha
            self.bannerBlackBG.isHidden = false
        }
        
    }
    
}

extension CategoryDetailViewController: ProductViewControllerProtocol, CategoryCollectionReusableViewProtocol{
    
    func updateHeaderHeight(height: CGFloat) {
        heightOfDescription = height
    }
    
    func populateCategory(category: Category) {
        self.category = category
        self.stemsList.removeAll()
        stemsCollectionView.reloadData()
        self.getCategoryProductsFromRemote()
    }
}

extension CategoryDetailViewController: UITextFieldDelegate, SearchViewControllerProtocol{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) {
            self.searchCancelButton.alpha = 1
            self.searchFieldXConstraint.constant = -27
            self.searchFieldYConstraint.constant = 450
            self.view.layoutIfNeeded()
            
        }
    }
    
    func didSelectedStem(stem: Stem) {
        populateCategory(category: Utils.shared.getCategoryByCategoryId(categoryId: stem.categoryId)!)
        stemToBeSent = stem
        performSegue(withIdentifier: String(describing: ProductViewController.self), sender: self)
    }
    
}


