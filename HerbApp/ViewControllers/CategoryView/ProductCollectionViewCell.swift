//
//  ProductCollectionViewCell.swift
//  HerbApp
//
//  Created by Shoaib Ismail on 18/07/2018.
//  Copyright © 2018 Emblem Technologies. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet var ivProduct: UIImageView!
    @IBOutlet var productLabel: UILabel!
    @IBOutlet weak var lblTHCValue: UILabel!
    
    func populateCell(stem:Stem){
        Utils.shared.loadImage(imageView: ivProduct, urlString: stem.imagesList.first?.imageURL ?? "", placeHolderImageString: "PlaceHolder")
        productLabel.text = stem.name
        lblTHCValue.text = stem.thc
    }
    
}
