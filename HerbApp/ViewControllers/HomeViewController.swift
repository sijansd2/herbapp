//
//  HomeViewController.swift
//  HerbApp
//
//  Created by Shahbaz Saleem on 7/17/18.
//  Copyright © 2018 Emblem Technologies. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var searchBarTopConstraint: NSLayoutConstraint!
    @IBOutlet var searchBarBGView: UIView!
    @IBOutlet weak var ivLogo: UIImageView!
    @IBOutlet weak var indicaButton: UIButton!
    @IBOutlet weak var sativaButton: UIButton!
    @IBOutlet weak var hybridButton: UIButton!
    @IBOutlet var searchStemsTableView: UITableView!

    
    var timer: Timer!
    var searchTextCount = 0
    
    static var catagoriesList = Array<Category>()
    var stemsList = Array<Stem>()
    
    var selectedCategoryToBeSent: Category?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ivLogo.alpha = 0
        getCategoriesListingFromRemote()
    }
 

    @objc func startAnimating(){
        UIView.animate(withDuration: 1.5) {
            self.searchBar.transform = CGAffineTransform.init(translationX: 0, y: 0)
            self.ivLogo.transform = CGAffineTransform.init(translationX: 0, y: 0)
            self.indicaButton.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            self.sativaButton.transform = self.indicaButton.transform
            self.hybridButton.transform = self.indicaButton.transform
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CategoryDetailViewController{
            vc.category = selectedCategoryToBeSent!
        }
    }
    
    
    
    func  populateData()
    {
        var loopCount = 1;
        for category in HomeViewController.catagoriesList {
            if(loopCount==1){
                indicaButton.setTitle(category.name, for: .normal)
            }
            if(loopCount==2){
                sativaButton.setTitle(category.name, for: .normal)
            }
            if(loopCount==3){
                hybridButton.setTitle(category.name , for: .normal)
            }
            loopCount = loopCount + 1
        }
        self.ivLogo.isHidden = false
        self.searchBar.isHidden = false
        self.indicaButton.isHidden = false
        self.sativaButton.isHidden = false
        self.hybridButton.isHidden = false
        
    // MARK: - Layout Animation
        ivLogo.transform = CGAffineTransform.init(translationX: 0, y: (view.frame.height*0.5)-(ivLogo.frame.height*0.5)-ivLogo.frame.minY)
        searchBar.transform = CGAffineTransform.init(translationX: 0, y: 200)
        indicaButton.transform = CGAffineTransform.init(scaleX: 0, y: 0)
        sativaButton.transform = indicaButton.transform
        hybridButton.transform = indicaButton.transform
        
        UIView.animate(withDuration: 0) {
            self.ivLogo.alpha = 1
        }
        
        perform(#selector(self.startAnimating), with: self, afterDelay: 0.3)
    }
    
// MARK: - UIActions
    @IBAction func didSelectCategoryButton(_ sender: UIButton) {
        switch sender {
        case self.indicaButton:
            self.selectedCategoryToBeSent = HomeViewController.catagoriesList.first
            break
        case self.sativaButton:
            self.selectedCategoryToBeSent = HomeViewController.catagoriesList[1]
            break
        case self.hybridButton:
            self.selectedCategoryToBeSent = HomeViewController.catagoriesList[2]
            break
        default:
            break
        }
        if(selectedCategoryToBeSent != nil){
            performSegue(withIdentifier: String(describing: CategoryDetailViewController.self), sender: self)
        }
    }
    
    
    // MARK: - APIs
    func getCategoriesListingFromRemote(){
        APIHandler.shared.getCategories(onSuccess: { (response) in
            if let responseDic = response as? Dictionary<String,Any>{
                if let items = responseDic[APIKey.items] as? Array<Dictionary<String,Any>>{
                    HomeViewController.catagoriesList.removeAll()
                    items.forEach({ (categoryDic) in
                        HomeViewController.catagoriesList.append(Category(categoryDic))
                    })
                    self.populateData()
                }
            }
        }) { (message, errorCode, response) in
            
        }
    }
    
    func getStems(searchString: String){
        APIHandler.shared.getStems(searchString: searchString.lowercased(), onSuccess: { (response) in
            if let responseDic = response as? Dictionary<String,Any>{
                if let items = responseDic[APIKey.items] as? Array<Dictionary<String,Any>>{
                    self.stemsList.removeAll()
                    items.forEach({ (stemDic) in
                        self.stemsList.append(Stem(stemDic))
                    })
                    self.searchStemsTableView.reloadData()
                }
            }
        }) { (message, code, response) in
            
        }
    }

}

extension HomeViewController: UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        UIView.animate(withDuration: 0.3) {
            self.searchBarBGView.alpha = 1
            self.searchBarTopConstraint.isActive = true
            self.view.layoutIfNeeded()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(searchTextCount != searchBar.text?.count){
            searchTextCount = (searchBar.text?.count)!
            if(timer != nil){
                timer.invalidate()
            }
            timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(updateSearchViewController), userInfo: nil, repeats: false)
            
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = nil
        stemsList = Array<Stem>()
        searchStemsTableView.reloadData()
        searchBar.setShowsCancelButton(false, animated: false)
        UIView.animate(withDuration: 0.5) {
            self.searchBarBGView.alpha = 0
            self.searchBarTopConstraint.isActive = false
            self.view.layoutIfNeeded()
            
        }
    }
    
    @objc func updateSearchViewController(){
        if(searchBar.text != nil && searchBar.text != ""){
            self.getStems(searchString: searchBar.text!)
        }else{
            stemsList = Array<Stem>()
            searchStemsTableView.reloadData()
        }
        
    }
    
    
}


extension HomeViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stemsList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let rowHeight: Int = 50
        return CGFloat(rowHeight)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SuggestionsTableViewCell.self), for: indexPath) as! SuggestionsTableViewCell
        
        cell.textLabel?.text = stemsList[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let stem = stemsList[indexPath.row]
        let category = Utils.shared.getCategoryByCategoryId(categoryId: stem.categoryId)
        if(category != nil){
            let categoryDetailViewController = storyboard?.instantiateViewController(withIdentifier: String(describing: CategoryDetailViewController.self)) as! CategoryDetailViewController
            
            categoryDetailViewController.category = category!
            
            let productViewController = storyboard?.instantiateViewController(withIdentifier: String(describing: ProductViewController.self)) as! ProductViewController
            productViewController.delegate = categoryDetailViewController
            productViewController.stem = stem
            
            navigationController?.pushViewController(categoryDetailViewController, animated: false)
            navigationController?.pushViewController(productViewController, animated: true)
            
        }
        
        
    }
    
    
    
}

