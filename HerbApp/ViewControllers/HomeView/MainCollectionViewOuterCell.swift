//
//  MainCollectionViewOuterCell.swift
//  HerbApp
//
//  Created by Shahbaz Saleem on 27/07/2018.
//  Copyright © 2018 Emblem Technologies. All rights reserved.
//

import UIKit

protocol MainCollectionViewOuterCellProtocol {
    func didSelectInnerCell(category: Category, stem: Stem)
}
class MainCollectionViewOuterCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var innerCollectionView: UICollectionView!
    
    var delegate: MainCollectionViewOuterCellProtocol?
    var category: Category?
    
    func populateData(category: Category?){
        self.category = category
        
        innerCollectionView.reloadData()
        categoryName.text = category?.name
    }
    
}

extension MainCollectionViewOuterCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return category?.stemsList.count ?? 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfCells = (Utils.shared.isiPhone ? 2 : 4) as CGFloat
        let spacingBetweenCells = (20*numberOfCells) as CGFloat
        let widthOfInnerCell = (collectionView.frame.width - 40 - spacingBetweenCells - 20)/numberOfCells
//        let widthOfInnerCell = (collectionView.frame.width - 40 - 40 - 20)/2
        let heightOfImage = widthOfInnerCell
        
        return CGSize.init(width: widthOfInnerCell, height: heightOfImage + 23)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MainCollectionViewInnerCell.self), for: indexPath) as! MainCollectionViewInnerCell
        
        cell.populateData(stem: category?.stemsList[indexPath.item])
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(category != nil){
            delegate?.didSelectInnerCell(category: category!, stem: category!.stemsList[indexPath.item])
        }
    }
    
}
