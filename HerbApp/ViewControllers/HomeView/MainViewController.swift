//
//  MainViewController.swift
//  HerbApp
//
//  Created by Shahbaz Saleem on 26/07/2018.
//  Copyright © 2018 Emblem Technologies. All rights reserved.
//

import UIKit
import AVKit

class MainViewController: UIViewController {

    @IBOutlet weak var videoVieo: UIView!
    @IBOutlet weak var categoriesOuterCollectionView: UICollectionView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchContainer: UIView!
    @IBOutlet weak var searchCancelButton: UIButton!
    
    @IBOutlet weak var searchFieldXConstraint: NSLayoutConstraint!
    @IBOutlet weak var videoViewIpadConstraint: NSLayoutConstraint!
    @IBOutlet weak var videoViewiPhoneConstraint: NSLayoutConstraint!
    
    var searchViewController: SearchViewController?
    
    var playerLooper: NSObject?
    var playerLayer:AVPlayerLayer!
    var queuePlayer: AVQueuePlayer?
    
    static var categoriesList = Array<Category>()
    var selectedCategoryToBeSent: Category?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        perform(#selector(self.openAgeAlertView), with: self, afterDelay: 0.3)
        playVideo("video")
        categoriesOuterCollectionView.contentInset = UIEdgeInsetsMake(30, 0, 0, 0)
        searchContainer.alpha = 0
        searchField.setLeftPaddingPoints(23)
        videoViewiPhoneConstraint.isActive = Utils.shared.isiPhone
        self.view.layoutIfNeeded()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getCategoriesListingFromRemote()
         self.view.layoutIfNeeded()
    }
    
    @objc func openAgeAlertView(){
        performSegue(withIdentifier: String(describing: AgeAlertViewController.self), sender: self)
    }
    
    func playVideo(_ filmName: String){
        if let path = Bundle.main.path(forResource: filmName, ofType: "mp4") {
            let url =  URL(fileURLWithPath: path)
            
            let playerItem = AVPlayerItem(url: url as URL)
            self.queuePlayer = AVQueuePlayer(items: [playerItem])

            playerLooper = AVPlayerLooper(player: self.queuePlayer!, templateItem: playerItem)
            
            self.playerLayer = AVPlayerLayer(player: self.queuePlayer)
            self.playerLayer.videoGravity = .resizeAspectFill
            self.videoVieo.layer.addSublayer(self.playerLayer!)
            self.playerLayer?.frame = self.videoVieo.frame
            self.queuePlayer?.play()
            
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CategoryDetailViewController{
            vc.category = selectedCategoryToBeSent!
        }else if let vc = segue.destination as? SearchViewController{
            searchViewController = vc
            vc.delegate = self
        }
    }

    // MARK: - APIs
    func getCategoriesListingFromRemote(){
        APIHandler.shared.getCategories(onSuccess: { (response) in
            if let responseDic = response as? Dictionary<String,Any>{
                if let items = responseDic[APIKey.items] as? Array<Dictionary<String,Any>>{
                    MainViewController.categoriesList.removeAll()
                    items.forEach({ (categoryDic) in
                        MainViewController.categoriesList.append(Category(categoryDic))
                    })
                    
                    MainViewController.categoriesList.forEach({ (category) in
                        self.getStemsFromRemote(category: category)
                    })
                    
                }
            }
        }) { (message, errorCode, response) in
            
        }
    }
    
    func getStemsFromRemote(category: Category){
        APIHandler.shared.getCategoryProducts(categoryId: category.id, onSuccess: { (response) in
            if let responseDic = response as? Dictionary<String,Any>{
                if let items = responseDic[APIKey.items] as? Array<Dictionary<String,Any>>{
                    category.stemsList.removeAll()
                    items.forEach({ (stemDic) in
                        category.stemsList.append(Stem(stemDic))
                    })
                    self.reloadCollectionViewIfNeeded()
                }
            }
        }) { (message, code, response) in
            
        }
    }
    
    func reloadCollectionViewIfNeeded(){
        var isAllCategoriesLoaded = true
        for category in MainViewController.categoriesList {
            if(category.stemsList.count == 0){
                isAllCategoriesLoaded = false
                break
            }
        }
        
        if(isAllCategoriesLoaded){
            categoriesOuterCollectionView.reloadData()
            categoriesOuterCollectionView.scrollToItem(at: IndexPath.init(item: 0, section: 0), at: .top, animated: true)
        }
    }
    
    @IBAction func searchTextFieldEditingChanged(_ sender: UITextField) {
        
        searchViewController?.didSearchUpdated(searchString: sender.text ?? DefaultValue.string)
        
    }
    
    @IBAction func didSelectSearchCancel(_ sender: UIButton) {
        searchField.text = nil
        searchField.resignFirstResponder()
        searchViewController?.didSearchUpdated(searchString: searchField.text ?? DefaultValue.string)
        UIView.animate(withDuration: 0.3) {
            self.searchContainer.alpha = 0
            self.searchCancelButton.alpha = 0
            self.searchFieldXConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
}


extension MainViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MainCollectionViewOuterCellProtocol{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return MainViewController.categoriesList.count == 0 ? 3 : MainViewController.categoriesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfCells = (Utils.shared.isiPhone ? 2 : 4) as CGFloat
        let spacingBetweenCells = (20*numberOfCells) as CGFloat
        let widthOfInnerCell = (collectionView.frame.width - 40 - spacingBetweenCells - 20)/numberOfCells
        let heightOfImage = widthOfInnerCell
        return CGSize.init(width: collectionView.frame.width, height: heightOfImage+92)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MainCollectionViewOuterCell.self), for: indexPath) as! MainCollectionViewOuterCell
        let category = MainViewController.categoriesList.count > indexPath.item ? MainViewController.categoriesList[indexPath.item] : nil
        cell.delegate = self
        cell.populateData(category: category)
        
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCategoryToBeSent = MainViewController.categoriesList.count > indexPath.item ? MainViewController.categoriesList[indexPath.item] : nil
        if(selectedCategoryToBeSent != nil){
            performSegue(withIdentifier: String(describing: CategoryDetailViewController.self), sender: self)
        }
    }
    
    func didSelectInnerCell(category: Category, stem: Stem) {
        let categoryDetailViewController = storyboard?.instantiateViewController(withIdentifier: String(describing: CategoryDetailViewController.self)) as! CategoryDetailViewController
        
        categoryDetailViewController.category = category
        
        let productViewController = storyboard?.instantiateViewController(withIdentifier: String(describing: ProductViewController.self)) as! ProductViewController
        productViewController.delegate = categoryDetailViewController
        productViewController.stem = stem
        
        navigationController?.pushViewController(categoryDetailViewController, animated: false)
        navigationController?.pushViewController(productViewController, animated: true)
    }
    
}

extension MainViewController: UITextFieldDelegate, SearchViewControllerProtocol{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) {
            self.searchContainer.alpha = 1
            self.searchCancelButton.alpha = 1
            self.searchFieldXConstraint.constant = -27
            self.view.layoutIfNeeded()

        }
    }

    func didSelectedStem(stem: Stem) {
        let category = Utils.shared.getCategoryByCategoryId(categoryId: stem.categoryId)
        if(category != nil){
            let categoryDetailViewController = storyboard?.instantiateViewController(withIdentifier: String(describing: CategoryDetailViewController.self)) as! CategoryDetailViewController
            
            categoryDetailViewController.category = category!
            
            let productViewController = storyboard?.instantiateViewController(withIdentifier: String(describing: ProductViewController.self)) as! ProductViewController
            productViewController.delegate = categoryDetailViewController
            productViewController.stem = stem
            
            navigationController?.pushViewController(categoryDetailViewController, animated: false)
            navigationController?.pushViewController(productViewController, animated: true)
        }
    }
}

