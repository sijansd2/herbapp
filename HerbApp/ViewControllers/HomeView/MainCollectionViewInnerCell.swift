//
//  MainCollectionViewInnerCell.swift
//  HerbApp
//
//  Created by Shahbaz Saleem on 27/07/2018.
//  Copyright © 2018 Emblem Technologies. All rights reserved.
//

import UIKit

class MainCollectionViewInnerCell: UICollectionViewCell {
    @IBOutlet weak var ivThumbnail: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    var stem: Stem?
    
    func populateData(stem: Stem?){
        self.stem = stem
        
        Utils.shared.loadImage(imageView: ivThumbnail, urlString: stem?.imagesList.first?.imageURL ?? DefaultValue.string, placeHolderImageString: "PlaceHolderSquare")
        lblName.text = stem?.name
        
    }
    
}
