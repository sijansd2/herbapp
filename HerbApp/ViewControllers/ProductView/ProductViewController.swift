//
//  ProductViewController.swift
//  HerbApp
//
//  Created by Shoaib Ismail on 18/07/2018.
//  Copyright © 2018 Emblem Technologies. All rights reserved.
//

import UIKit

protocol ProductViewControllerProtocol {
    func populateCategory(category: Category)
}

class ProductViewController: UIViewController {

    @IBOutlet weak var ivStem: UIImageView!
    @IBOutlet weak var lblStemName: UILabel!
    @IBOutlet var categoryFirstButton: UIButton!
    @IBOutlet var categorySecButton: UIButton!
    @IBOutlet var categoryThirdButton: UIButton!
    @IBOutlet weak var productScrollView: UIScrollView!
    @IBOutlet weak var showMoreButton: UIButton!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchContainer: UIView!
    @IBOutlet weak var searchCancelButton: UIButton!
    @IBOutlet weak var tvDescription: UITextView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var lblTHCValue: UILabel!
    
    @IBOutlet weak var searchFieldXConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchFieldYConstraint: NSLayoutConstraint!
    @IBOutlet weak var tvDescriptionHConstraint: NSLayoutConstraint!
    @IBOutlet var categoryButtonsParentConstraingWithTVDescription: NSLayoutConstraint!
    
    var searchViewController: SearchViewController?
    var delegate: ProductViewControllerProtocol?
    

    
    var stem: Stem = Stem(nil)
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        setup()
        
    }
    
    private func setup(){
        let imgRect = UIBezierPath.init(rect: CGRect.init(x: 0, y: 0, width: ivStem.frame.width, height: ivStem.frame.height))
        self.tvDescription.textContainer.exclusionPaths = [imgRect];
        searchField.setLeftPaddingPoints(23)
        populateViews()
        
    }
    
    @objc private func populateViews(){
        backButton.setTitle(stem.categoryName, for: .normal)
        lblStemName.text = stem.name
        lblTHCValue.text = stem.thc
        tvDescription.text = stem.description
        Utils.shared.loadImage(imageView: ivStem, urlString: stem.imagesList.first?.imageURL ?? DefaultValue.string, placeHolderImageString: "PlaceHolderBanner")
        
        categoryFirstButton.setTitle(MainViewController.categoriesList.first?.name ?? DefaultValue.string, for: .normal)
        if(MainViewController.categoriesList.count > 1){
            categorySecButton.setTitle(MainViewController.categoriesList[1].name , for: .normal)
        }
        
        if(MainViewController.categoriesList.count > 2){
            categoryThirdButton.setTitle(MainViewController.categoriesList[2].name , for: .normal)
        }
        
        tvDescriptionHConstraint.constant = tvDescription.sizeThatFits(tvDescription.frame.size).height
        self.view.layoutIfNeeded()
        
        categoryButtonsParentConstraingWithTVDescription.isActive = ivStem.frame.maxY < tvDescription.frame.maxY
        self.view.layoutIfNeeded()
        
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SearchViewController{
            searchViewController = vc
            vc.delegate = self
        }
    }
    // MARK: - UIActions
    
//    @IBAction func showMoreButtonTapped(_ sender: UIButton) {
//        if sender.tag == 0{
//            productScrollView.isScrollEnabled = true
//            productScrollView.showsVerticalScrollIndicator = true
//            lblDescription.numberOfLines = 0
//            showMoreButton.setTitle("ShowLess", for: .normal)
//            sender.tag = 1
//        }else{
//            productScrollView.setContentOffset(.zero, animated: true)
//            productScrollView.scrollsToTop = true
//            productScrollView.isScrollEnabled = false
//            lblDescription.numberOfLines = 5
//            showMoreButton.setTitle("ShowMore", for: .normal)
//            sender.tag = 0
//        }
//    }
    
    @IBAction func didSelectFirstCategoryButton(_ sender: UIButton) {
        delegate?.populateCategory(category: MainViewController.categoriesList.first ?? Category(nil))
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didSelectSecondCategoryButton(_ sender: UIButton) {
        if(MainViewController.categoriesList.count > 1){
            delegate?.populateCategory(category: MainViewController.categoriesList[1])
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func didSelectThirdCategoryButton(_ sender: UIButton) {
        if(MainViewController.categoriesList.count > 2){
            delegate?.populateCategory(category: MainViewController.categoriesList[2])
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func searchTextFieldEditingChanged(_ sender: UITextField) {
        
        searchViewController?.didSearchUpdated(searchString: sender.text ?? DefaultValue.string)
        
    }
    
    @IBAction func didSelectSearchCancel(_ sender: UIButton) {
        searchField.text = nil
        searchField.resignFirstResponder()
        searchViewController?.didSearchUpdated(searchString: searchField.text ?? DefaultValue.string)
        UIView.animate(withDuration: 0.3) {
            self.searchCancelButton.alpha = 0
            self.searchFieldXConstraint.constant = 0
            self.searchFieldYConstraint.constant = 20
            self.view.layoutIfNeeded()
        }
    }

}


extension ProductViewController: UITextFieldDelegate, SearchViewControllerProtocol{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) {
            self.searchCancelButton.alpha = 1
            self.searchFieldXConstraint.constant = -27
            self.searchFieldYConstraint.constant = 450
            self.view.layoutIfNeeded()
            
        }
    }
    
    func didSelectedStem(stem: Stem) {
        self.stem = stem
        delegate?.populateCategory(category: Utils.shared.getCategoryByCategoryId(categoryId: stem.categoryId)!)
        didSelectSearchCancel(searchCancelButton)
        self.populateViews()
    }
    
}
