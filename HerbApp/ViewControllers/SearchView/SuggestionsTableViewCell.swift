//
//  SuggestionsTableViewCell.swift
//  HerbApp
//
//  Created by Shoaib Ismail on 17/07/2018.
//  Copyright © 2018 Emblem Technologies. All rights reserved.
//

import UIKit

class SuggestionsTableViewCell: UITableViewCell {

    @IBOutlet var ivProduct: UIImageView!
    @IBOutlet var productLabel: UILabel!
    @IBOutlet weak var lblTHCValue: UILabel!
    
    func populateCell(stem:Stem){
        Utils.shared.loadImage(imageView: ivProduct, urlString: stem.imagesList.first?.imageURL ?? "", placeHolderImageString: "PlaceHolder")
        productLabel.text = stem.name
        lblTHCValue.text = stem.thc
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
