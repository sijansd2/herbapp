//
//  SearchViewController.swift
//  HerbApp
//
//  Created by Shahbaz Saleem on 27/07/2018.
//  Copyright © 2018 Emblem Technologies. All rights reserved.
//

import UIKit

protocol SearchViewControllerProtocol {
    func didSelectedStem(stem: Stem)
}

class SearchViewController: UIViewController {

    
    @IBOutlet var searchStemsTableView: UITableView!
    
    var delegate: SearchViewControllerProtocol?
    var timer: Timer!
    var searchTextCount = 0
    var searchString = DefaultValue.string
    
    var stemsList = Array<Stem>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    func didSearchUpdated(searchString: String){
        self.searchString = searchString
        if(searchTextCount != searchString.count){
            searchTextCount = (searchString.count)
            if(timer != nil){
                timer.invalidate()
            }
            timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(updateSearch), userInfo: nil, repeats: false)
            
        }
    }
    
    @objc func updateSearch(){
        if(searchString != DefaultValue.string){
            self.getStems(searchString: searchString)
        }else{
            stemsList = Array<Stem>()
            searchStemsTableView.reloadData()
        }
        
    }
    
    func getStems(searchString: String){
        APIHandler.shared.getStems(searchString: searchString.lowercased(), onSuccess: { (response) in
            if let responseDic = response as? Dictionary<String,Any>{
                if let items = responseDic[APIKey.items] as? Array<Dictionary<String,Any>>{
                    self.stemsList.removeAll()
                    items.forEach({ (stemDic) in
                        self.stemsList.append(Stem(stemDic))
                    })
                    self.searchStemsTableView.reloadData()
                }
            }
        }) { (message, code, response) in
            
        }
    }
}

extension SearchViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stemsList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let rowHeight: Int = 53
        return CGFloat(rowHeight)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SuggestionsTableViewCell.self), for: indexPath) as! SuggestionsTableViewCell
        
        cell.populateCell(stem: stemsList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        delegate?.didSelectedStem(stem: stemsList[indexPath.row])
            
    }
    
}
